//
//  SwiftConcurrency2024App.swift
//  SwiftConcurrency2024
//
//  Created by Karla Pangilinan on 6/22/24.
//

import SwiftUI

@main
struct SwiftConcurrency2024App: App {
  var body: some Scene {
    WindowGroup {
      ContentView()
    }
  }
}
