//
//  DownloadImageAsync.swift
//  SwiftConcurrency2024
//
//  Created by Karla Pangilinan on 6/23/24.
//

import Combine
import SwiftUI

// MARK: - Image Loader

class DownloadImageAsyncImageLoader {
  let url = URL(string: "https://picsum.photos/200")!

  func handleResponse(data: Data?, response: URLResponse?) -> UIImage? {
    guard
      let data = data,
      let image = UIImage(data: data),
      let response = response as? HTTPURLResponse,
      response.statusCode >= 200 && response.statusCode < 300 else {
      return nil
    }

    return image
  }

  func downloadWithEscaping(completionHandler: @escaping (_ image: UIImage?, _ error: Error?) -> ()) {
    URLSession.shared.dataTask(with: url) { [weak self] data, response, error in
      let image = self?.handleResponse(data: data, response: response)
      completionHandler(image, error)
    }
    .resume()
  }

  func downloadWithCombine() -> AnyPublisher<UIImage?, Error> {
    URLSession.shared.dataTaskPublisher(for: url)
      .receive(on: DispatchQueue.main)
      .map(handleResponse)
      .mapError { $0 }
      .eraseToAnyPublisher()
  }

  func downloadWithAsync() async throws -> UIImage? {
    do {
      let (data, response) = try await URLSession.shared.data(from: url)
      let image = handleResponse(data: data, response: response)
      return image
    } catch {
      throw error
    }
  }
}

// MARK: - View Model

class DownloadImageAsyncViewModel: ObservableObject {
  @Published var image: UIImage? = nil

  let loader = DownloadImageAsyncImageLoader()

  private var cancellables = Set<AnyCancellable>()

  func fetchImage() {
    loader.downloadWithEscaping { [weak self] image, error in
      if let image = image {
        DispatchQueue.main.async {
          self?.image = image
        }
      }
    }
  }

  func fetchImageCombine() {
    loader.downloadWithCombine()
      .sink { _ in

      } receiveValue: { image in
        self.image = image
      }
      .store(in: &cancellables)

  }

  func fetchImageAsync() async {
    let image = try? await loader.downloadWithAsync()
    
    /// handles publishing changes to the main thread when using `async`
    await MainActor.run {
      self.image = image
    }
  }
}

// MARK: - View

struct DownloadImageAsync: View {
  @StateObject private var viewModel = DownloadImageAsyncViewModel()

  var body: some View {
    ZStack {
      if let image = viewModel.image {
        Image(uiImage: image)
          .resizable()
          .scaledToFit()
          .frame(width: 250, height: 250)
      }
    }
    .onAppear() {
//      viewModel.fetchImage()
//      viewModel.fetchImageCombine()
      Task {
        await viewModel.fetchImageAsync()
      }
    }
  }
}

#Preview {
  DownloadImageAsync()
}
