//
//  AsyncAwait.swift
//  SwiftConcurrency2024
//
//  Created by Karla Pangilinan on 6/23/24.
//

import SwiftUI

// MARK: - View Model

class AsyncAwaitViewModel: ObservableObject {
  @Published var dataArray: [String] = []

  func addTitle1() {
    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
      self.dataArray.append("Title 1: \(Thread.current)")
    }
  }

  func addTitle2() {
    DispatchQueue.global().asyncAfter(deadline: .now() + 2) {
      let title = "Title 2: \(Thread.current)"
      DispatchQueue.main.async {
        self.dataArray.append(title)

        let title3 = "Title 3: \(Thread.current)"
        self.dataArray.append(title3)
      }
    }
  }

  // executes the lines in order (top to bottom) even if it's async code
  func addAuthor1() async {
    let author1 = "Author 1: \(Thread.current)"
    //self.dataArray.append(author1) // Threading error
    await MainActor.run {
      self.dataArray.append(author1)
    }

    try? await Task.sleep(nanoseconds: 2_000_000_000)

    let author2 = "Author 2: \(Thread.current)"

    await MainActor.run {
      self.dataArray.append(author2)

      let author3 = "Author 3: \(Thread.current)"
      self.dataArray.append(author3)
    }

    await addSomething()
  }

  func addSomething() async {
    try? await Task.sleep(nanoseconds: 2_000_000_000)

    let something1 = "Something 1: \(Thread.current)"

    await MainActor.run {
      self.dataArray.append(something1)

      let something2 = "Something 2: \(Thread.current)"
      self.dataArray.append(something2)
    }
  }
}

struct AsyncAwait: View {
  @StateObject private var viewModel = AsyncAwaitViewModel()

  var body: some View {
    List {
      ForEach(viewModel.dataArray, id: \.self) { data in
        Text(data)
      }
    }
    .onAppear() {
//      viewModel.addTitle1()
//      viewModel.addTitle2()

      Task {
        await viewModel.addAuthor1()

        let finalText = "FINAL TEXT: \(Thread.current)"
        viewModel.dataArray.append(finalText)
      }
    }
  }
}

#Preview {
  AsyncAwait()
}
