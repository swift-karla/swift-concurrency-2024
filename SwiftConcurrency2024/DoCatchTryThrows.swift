//
//  DoCatchTryThrows.swift
//  SwiftConcurrency2024
//
//  Created by Karla Pangilinan on 6/23/24.
//

import SwiftUI

/// do-catch try throws

class DoCatchTryThrowsDataManager {

  let isActive: Bool = false

  func getTitle() -> String? {
    if isActive {
      return "NEW TEXT!"
    } else {
      return nil
    }
  }

  func getTitle2() -> Result<String, Error> {
    if isActive {
      return .success("NEW TEXT!")
    } else {
      return .failure(URLError(.unknown))
    }
  }

  func getTitle3() throws -> String {
    if isActive {
      return "NEW TEXT!"
    } else {
      throw URLError(.badServerResponse)
    }
  }

  func getTitle4() throws -> String {
    if isActive {
      return "NEW TEXT!!"
    } else {
      throw URLError(.badServerResponse)
    }
  }
}

// MARK: - View Model

class DoCatchTryThrowsViewModel: ObservableObject {
  @Published var text: String = "Starting text."

  let manager = DoCatchTryThrowsDataManager()

  func fetchTitle() {
    do {
      self.text = try manager.getTitle3()

      let title = try manager.getTitle4()
      print(title)
    } catch {
      self.text = error.localizedDescription
    }
  }
}

// MARK: - View

struct DoCatchTryThrows: View {
  @StateObject private var viewModel = DoCatchTryThrowsViewModel()

  var body: some View {
    Text(viewModel.text)
      .frame(width: 300, height: 300)
      .background(Color.blue)
      .onTapGesture {
        viewModel.fetchTitle()
      }
  }
}

#Preview {
  DoCatchTryThrows()
}
